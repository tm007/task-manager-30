package ru.tsc.apozdnov.tm.api.component;

import org.jetbrains.annotations.NotNull;

public interface ISaltProvider {

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getPasswordIteration();

}
