package ru.tsc.apozdnov.tm.exception.system;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public final class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException() {
        super("FAULT!! Argument Not Supported");
    }

    public ArgumentNotSupportedException(final String arg) {
        super("FAULT!!! Argument " + arg + " is not supported!");
    }

}
