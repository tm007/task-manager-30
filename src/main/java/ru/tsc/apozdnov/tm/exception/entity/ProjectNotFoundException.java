package ru.tsc.apozdnov.tm.exception.entity;

public final class ProjectNotFoundException extends AbstractEntityNotFoundException {

    public ProjectNotFoundException() {
        super("FAULT!Project not found!!!");
    }

}
