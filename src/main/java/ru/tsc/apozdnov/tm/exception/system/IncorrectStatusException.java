package ru.tsc.apozdnov.tm.exception.system;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public final class IncorrectStatusException extends AbstractException {

    public IncorrectStatusException(final String value) {
        super("Error! Incorrect status. Value `" + value + "` not found!");
    }

}