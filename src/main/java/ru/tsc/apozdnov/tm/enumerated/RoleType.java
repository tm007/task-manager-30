package ru.tsc.apozdnov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum RoleType {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

    RoleType(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }
}
