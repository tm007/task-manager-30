package ru.tsc.apozdnov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.api.model.ICommand;

@Setter
@Getter
@NoArgsConstructor
public class Command implements ICommand {

    @NotNull
    private String name = "";

    @NotNull
    private String argument = "";

    @NotNull
    private String description = "";

    @NotNull
    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
