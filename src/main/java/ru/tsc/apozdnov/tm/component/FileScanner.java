package ru.tsc.apozdnov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FileScanner {

    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    private final File folder = new File("./");

    private final List<String> commands = new ArrayList<>();

    private Bootstrap bootstrap;

    public FileScanner(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    private void init() {
        final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getCommandsWithArgument();
        commands.forEach(e -> this.commands.add(e.getName()));
        executorService.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

    private void process() {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    bootstrap.processCommandTask(fileName);
                } catch (Exception e) {
                    bootstrap.getLoggerService().error(e);
                } finally {
                    file.delete();
                }
            }
        }
    }

    public void start() {
        init();
    }

}
