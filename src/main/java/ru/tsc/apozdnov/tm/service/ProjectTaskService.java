package ru.tsc.apozdnov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.api.repository.IProjectRepository;
import ru.tsc.apozdnov.tm.api.repository.ITaskRepository;
import ru.tsc.apozdnov.tm.api.service.IProjectTaskService;
import ru.tsc.apozdnov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.apozdnov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.apozdnov.tm.exception.field.IdEmptyException;
import ru.tsc.apozdnov.tm.exception.user.UserIdEmptyException;
import ru.tsc.apozdnov.tm.model.Project;
import ru.tsc.apozdnov.tm.model.Task;

import java.util.List;
import java.util.Optional;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(@NotNull IProjectRepository projectRepository, @NotNull ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findOneById(taskId)).orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskFromProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new IdEmptyException();
        if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
        @NotNull final Task task = Optional.ofNullable(taskRepository.findOneById(taskId)).orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
    }

    @Override
    public void removeProjectById(@NotNull final String userdId, @NotNull final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        final Project project = Optional.ofNullable(projectRepository.findOneById(projectId)).orElseThrow(ProjectNotFoundException::new);
        projectRepository.remove(project);
        taskRepository.findAllByProjectId(userdId, projectId)
                .stream()
                .forEach(task -> taskRepository.removeById(userdId, task.getId()));
        projectRepository.removeById(userdId, projectId);
    }

}
